"""lsw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('daily/poses/', views.daily_pose, name='daily_pose'),
    path('daily/affirmations/', views.daily_affirmation, name='daily_affirmation'),
    path('daily/crystals/', views.daily_crystal, name='daily_crystal'),
    path('daily/meals/', views.daily_meal, name='daily_meal'),
    path('daily/oils/', views.daily_oil, name='daily_oil')
]
