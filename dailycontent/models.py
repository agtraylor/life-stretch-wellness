from django.core.files.storage import FileSystemStorage
from django.db import models

# Create your models here.
class Pose(models.Model):
    title = models.CharField(verbose_name="Pose Name", max_length=200, unique=True, 
                             blank=False)
    
    image = models.FileField(verbose_name="Pose Image", upload_to='poses/',
                             blank=True, null=True)
    
    feature_date = models.DateField(verbose_name="Date To Feature This Pose", 
                                    blank=True, null=True)
    
    description = models.TextField(verbose_name="Pose Description", max_length=5000, 
                                   blank=False)

    full_screen = models.BooleanField(verbose_name="Display Fullscreen?", blank=True, null=True)

class Affirmation(models.Model):
    summary = models.CharField(verbose_name="Affirmation Summary", max_length=200, unique=True, 
                             blank=False)
    
    image = models.FileField(verbose_name="Affirmation Image", upload_to='affirmations/',
                             blank=True, null=True)
    
    feature_date = models.DateField(verbose_name="Date To Feature This Affirmation", 
                                    blank=True, null=True)
    
    text = models.TextField(verbose_name="Affirmation Text", max_length=5000, 
                                   blank=False)

    full_screen = models.BooleanField(verbose_name="Display Fullscreen?", blank=True, null=True)

class Oil(models.Model):
    name = models.CharField(verbose_name="Oil Name", max_length=200, unique=True, 
                             blank=False)
    
    image = models.FileField(verbose_name="Oil Image", upload_to='oils/',
                             blank=True, null=True)
    
    feature_date = models.DateField(verbose_name="Date To Feature This Oil", 
                                    blank=True, null=True)
    
    text = models.TextField(verbose_name="Oil Description", max_length=5000, 
                                   blank=False)
    product_url = models.CharField(verbose_name="Product URL", max_length=500,
                                   blank=True)

    full_screen = models.BooleanField(verbose_name="Display Fullscreen?", blank=True, null=True)

class Crystal(models.Model):
    name = models.CharField(verbose_name="Crystal Name", max_length=200, unique=True, 
                             blank=False)
    
    image = models.FileField(verbose_name="Crystal Image", upload_to='crystals/',
                             blank=True, null=True)
    
    feature_date = models.DateField(verbose_name="Date To Feature This Crystal", 
                                    blank=True, null=True)
    
    text = models.TextField(verbose_name="Crystal Description", max_length=5000, 
                                   blank=False)

    full_screen = models.BooleanField(verbose_name="Display Fullscreen?", blank=True, null=True)

class Meal(models.Model):
    name = models.CharField(verbose_name="Meal Name", max_length=200, unique=True, 
                             blank=False)
    
    image = models.FileField(verbose_name="Meal Image", upload_to='meals/',
                             blank=True, null=True)
    
    feature_date = models.DateField(verbose_name="Date To Feature This Meal", 
                                    blank=True, null=True)
    
    text = models.TextField(verbose_name="Meal Description", max_length=5000, 
                                   blank=False)

    ingredients = models.TextField(verbose_name="List Of ingredients", max_length=5000, 
                                   blank=False)

    recipie = models.TextField(verbose_name="Recipie", max_length=5000, 
                                   blank=False)

    full_screen = models.BooleanField(verbose_name="Display Fullscreen?", blank=True, null=True)
