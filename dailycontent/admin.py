from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
from .models import Pose, Affirmation, Meal, Crystal, Oil

# Register your models here.

class PoseAdmin(admin.ModelAdmin):
    list_display = ('title', 'feature_date')
    search_fields = ('title',)

class AffirmationAdmin(SummernoteModelAdmin):
    list_display = ('summary', 'feature_date')
    summernote_fields = ('text',)
    search_fields = ('summary',)

class OilAdmin(admin.ModelAdmin):
    list_display = ('name', 'feature_date')
    search_fields = ('name',)

class CrystalAdmin(admin.ModelAdmin):
    list_display = ('name', 'feature_date')
    search_fields = ('name',)

class MealAdmin(admin.ModelAdmin):
    list_display = ('name', 'feature_date')
    search_fields = ('name',)

admin.site.register(Pose, PoseAdmin)
admin.site.register(Affirmation, AffirmationAdmin)
admin.site.register(Oil, OilAdmin)
admin.site.register(Crystal, CrystalAdmin)
admin.site.register(Meal, MealAdmin)
