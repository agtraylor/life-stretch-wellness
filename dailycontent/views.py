from django.shortcuts import render
from . import models
from datetime import date
import random
from django.contrib.auth.decorators import login_required

# Create your views here.

def daily_pose(request):
    y = date.today().year
    m = date.today().month
    d = date.today().day

    todays_pose = models.Pose.objects.all().filter(feature_date=date(y, m, d))

    if todays_pose.exists():
        pose = todays_pose.first()
    else:
        all_poses = models.Pose.objects.all()
        random.seed(y+m+d)
        pose = random.choice(all_poses)

    context = {
        'pose': pose
    }

    return render(request, 'pose.html', context)

def daily_affirmation(request):
    y = date.today().year
    m = date.today().month
    d = date.today().day

    todays_affirmation = models.Affirmation.objects.all().filter(feature_date=date(y, m, d))

    if todays_affirmation.exists():
        affirmation = todays_affirmation.first()
    else:
        all_affirmations = models.Affirmation.objects.all()
        random.seed(y+m+d)
        affirmation = random.choice(all_affirmations)

    context = {
        'affirmation': affirmation
    }

    return render(request, 'affirmation.html', context)

def daily_crystal(request):
    y = date.today().year
    m = date.today().month
    d = date.today().day

    todays_crystal = models.Crystal.objects.all().filter(feature_date=date(y, m, d))

    if todays_crystal.exists():
        affirmation = todays_affirmation.first()
    else:
        all_crystals = models.Crystal.objects.all()
        random.seed(y+m+d)
        crystal = random.choice(all_crystals)

    context = {
        'crystal': crystal
    }

    return render(request, 'crystal.html', context)

def daily_meal(request):
    y = date.today().year
    m = date.today().month
    d = date.today().day

    todays_meal = models.Meal.objects.all().filter(feature_date=date(y, m, d))

    if todays_meal.exists():
        meal = todays_meal.first()
    else:
        all_meals = models.Meal.objects.all()
        random.seed(y+m+d)
        meal = random.choice(all_meals)

    context = {
        'meal': meal
    }

    return render(request, 'meal.html', context)

def daily_oil(request):
    y = date.today().year
    m = date.today().month
    d = date.today().day

    todays_oil = models.Oil.objects.all().filter(feature_date=date(y, m, d))

    if todays_oil.exists():
        oil = todays_oil.first()
    else:
        all_oils = models.Oil.objects.all()
        random.seed(y+m+d)
        oil = random.choice(all_oils)

    context = {
        'oil': oil
    }

    return render(request, 'oil.html', context)