from django.apps import AppConfig


class DailyContentConfig(AppConfig):
    name = 'dailycontent'
    verbose_name = 'Daily Content'
